import '../css/style.css';
import { isPositiveInteger, updateCopyrightYear } from './utility';
import { Task, TodoList } from './model';
import ListProjectsView from './views/ListProjectsView';
import TasksView from './views/TasksView';
import AddTaskView from './views/AddTaskView';
import { DEFAULT_PROJECT_NAME } from './config';

const Controller = () => {
  let todolist = null;
  const listProjectsView = ListProjectsView();
  const tasksView = TasksView();
  const addTaskView = AddTaskView();

  const saveToLocalstorage = () => {
    const todos = todolist.getProjects().reduce((projects, project) => {
      const tasks = project.getTasks().reduce((allTasks, task) => {
        allTasks.push({
          title: task.getTitle(),
          description: task.getDescription(),
          dueDate: task.getDueDate(),
          priority: task.getPriority(),
          completed: task.isCompleted(),
        });
        return allTasks;
      }, []);
      projects.push({
        title: project.getTitle(),
        selected: project.isSelected(),
        tasks,
      });
      return projects;
    }, []);

    localStorage.setItem(
      'todolist',
      JSON.stringify({
        selectedProjectIndex: todolist.getSelectedProjectIndex(),
        todos,
      }),
    );
  };

  const loadFromLocalstorage = () => {
    const todosFromLocalstorage = localStorage.getItem('todolist');

    if (todosFromLocalstorage === null) {
      todolist = TodoList();
      todolist.addProject(DEFAULT_PROJECT_NAME);
    } else {
      const oldTodoList = JSON.parse(todosFromLocalstorage);
      const newTodoList = TodoList();

      oldTodoList.todos.forEach((project) => {
        newTodoList.addProject(project.title, project.selected);
        const projectIndex = newTodoList.projectsCount() - 1;

        project.tasks.forEach((task) => {
          const newTask = Task(
            task.title,
            task.description,
            task.dueDate,
            task.priority,
            task.completed,
          );
          newTodoList.addTask(projectIndex, newTask);
        });
      });

      newTodoList.setSelectedProjectIndex(oldTodoList.selectedProjectIndex);
      todolist = newTodoList;
    }
  };

  const hasProject = (projectIndex, projectTitle) => (
    todolist
      .getProjects()
      .some(
        (project, index) => index !== projectIndex && project.getTitle() === projectTitle,
      )
  );

  const isValidProject = (projectIndex, projectTitle) => {
    let isValid = true;

    if (!projectTitle) {
      listProjectsView.setCustomValidity('Please fill out this field.');
      listProjectsView.reportValidity();
      isValid = false;
    } else if (hasProject(projectIndex, projectTitle)) {
      listProjectsView.setCustomValidity('Project already exists.');
      listProjectsView.reportValidity();
      isValid = false;
    }

    return isValid;
  };

  const controlAddProject = () => {
    const projectTitle = listProjectsView.getAddProjectQuery();

    if (!isValidProject(-1, projectTitle)) {
      return;
    }

    const projectIndex = todolist.projectsCount() - 1;
    todolist.addProject(projectTitle, true);
    listProjectsView.setProjectSelected(projectIndex);
    listProjectsView.render(todolist);
    tasksView.render(todolist.getProject(projectIndex));
    saveToLocalstorage();
  };

  const controlEditProject = (projectIndex) => {
    const projectTitle = todolist.getProject(projectIndex).getTitle();
    listProjectsView.fillForm(projectTitle);
  };

  const controlUpdateProject = (projectIndex) => {
    const projectTitle = listProjectsView.getAddProjectQuery();

    if (!isValidProject(-1, projectTitle)) {
      return;
    }

    todolist.updateProject(projectIndex, projectTitle);
    listProjectsView.render(todolist);
    saveToLocalstorage();
  };

  const hasTask = (projectIndex, taskIndex, title) => (
    todolist
      .getProject(projectIndex)
      .getTasks()
      .some((task, index) => index !== taskIndex && task.getTitle() === title)
  );

  const isValidTask = (task, projectIndex, taskIndex) => {
    let isValid = true;

    if (task.getTitle().length === 0) {
      addTaskView.setCustomValidity('title', 'Please fill out this field.');
      addTaskView.reportValidity('title');
      isValid = false;
    } else if (hasTask(projectIndex, taskIndex, task.getTitle())) {
      addTaskView.setCustomValidity('title', 'Task already exists.');
      addTaskView.reportValidity('title');
      isValid = false;
    }

    if (task.getDueDate().length) {
      const isValidDate = Date.parse(task.getDueDate());

      if (!isValidDate) {
        addTaskView.setCustomValidity(
          'dueDate',
          'This is not a valid date format',
        );
        addTaskView.reportValidity('dueDate');
        isValid = false;
      }
    }

    return isValid;
  };

  const controlAddTask = (data) => {
    const currentSelectedProjectIndex = todolist.getSelectedProjectIndex();

    if (currentSelectedProjectIndex === -1) {
      return;
    }

    const task = Task(
      data.title,
      data.description,
      data.dueDate,
      data.priority,
    );

    if (!isValidTask(task, currentSelectedProjectIndex, -1)) {
      return;
    }

    todolist.addTask(currentSelectedProjectIndex, task);
    tasksView.render(todolist.getProject(currentSelectedProjectIndex));
    addTaskView.toggleWindow();
    saveToLocalstorage();
  };

  const controlSelectProject = (index) => {
    if (index === todolist.getSelectedProjectIndex()) {
      return;
    }
    if (todolist.getSelectedProjectIndex() !== -1) {
      listProjectsView.unsetProjectSelected(todolist.getSelectedProjectIndex());
    }
    todolist.setSelectedProjectIndex(index);
    listProjectsView.setProjectSelected(todolist.getSelectedProjectIndex());
    tasksView.render(todolist.getProject(index));
    saveToLocalstorage();
  };

  const controlDeleteProject = (index) => {
    if (!isPositiveInteger(index) || index >= todolist.projectsCount()) {
      return;
    }
    const oldSelectedProjectIndex = todolist.getSelectedProjectIndex();
    const isSelected = index === oldSelectedProjectIndex;
    todolist.deleteProject(index);
    const newSelectedProjectIndex = todolist.getSelectedProjectIndex();
    if (isSelected && newSelectedProjectIndex !== -1) {
      listProjectsView.setProjectSelected(newSelectedProjectIndex);
      tasksView.render(todolist.getProject(newSelectedProjectIndex));
    }
    listProjectsView.render(todolist);
    saveToLocalstorage();
  };

  const controlDeleteTask = (taskIndex) => {
    const currentSelectedProjectIndex = todolist.getSelectedProjectIndex();

    todolist.deleteTask(currentSelectedProjectIndex, taskIndex);
    tasksView.render(todolist.getProject(currentSelectedProjectIndex));
    saveToLocalstorage();
  };

  const controlCompleteTask = (taskIndex) => {
    const currentSelectedProjectIndex = todolist.getSelectedProjectIndex();

    const oldTask = todolist
      .getProject(currentSelectedProjectIndex)
      .getTask(taskIndex);
    const newTask = Task(
      oldTask.getTitle(),
      oldTask.getDescription(),
      oldTask.getDueDate(),
      oldTask.getPriority(),
      oldTask.isCompleted(),
    );

    newTask.toggleCompleted();
    todolist.updateTask(currentSelectedProjectIndex, taskIndex, newTask);
    tasksView.render(todolist.getProject(currentSelectedProjectIndex));
    saveToLocalstorage();
  };

  const controlEditTask = (taskIndex) => {
    const currentSelectedProjectIndex = todolist.getSelectedProjectIndex();

    const oldTask = todolist
      .getProject(currentSelectedProjectIndex)
      .getTask(taskIndex);

    addTaskView.fillForm(oldTask);
    addTaskView.toggleWindow();
  };

  const controlUpdateTask = (data, taskIndex) => {
    const currentSelectedProjectIndex = todolist.getSelectedProjectIndex();

    if (currentSelectedProjectIndex === -1) {
      return;
    }

    const task = Task(
      data.title,
      data.description,
      data.dueDate,
      data.priority,
    );

    if (!isValidTask(task, currentSelectedProjectIndex, taskIndex)) {
      return;
    }

    todolist.updateTask(currentSelectedProjectIndex, taskIndex, task);
    tasksView.render(todolist.getProject(currentSelectedProjectIndex));
    addTaskView.toggleWindow();
    saveToLocalstorage();
  };

  const init = () => {
    updateCopyrightYear('#copyrightYear');

    loadFromLocalstorage();
    listProjectsView.render(todolist);
    tasksView.render(todolist.getProject(todolist.getSelectedProjectIndex()));

    listProjectsView.addHandlerSelectProject(controlSelectProject);
    listProjectsView.addHandlerDeleteProject(controlDeleteProject);
    listProjectsView.addHandlerAddProject(controlAddProject);
    listProjectsView.addHandlerEditProject(controlEditProject);
    listProjectsView.addHandlerUpdateProject(controlUpdateProject);
    addTaskView.addHandlerSubmit(controlAddTask);
    addTaskView.addHandlerUpdate(controlUpdateTask);
    tasksView.addHandlerDeleteTask(controlDeleteTask);
    tasksView.addHandlerCompleteTask(controlCompleteTask);
    tasksView.addHandlerEditTask(controlEditTask);
  };

  return {
    init,
  };
};

const controller = Controller();

controller.init();
