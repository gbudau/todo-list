export const TaskPriority = Object.freeze({
  LOW: Symbol('low'),
  MEDIUM: Symbol('medium'),
  HIGH: Symbol('high'),
});

export const Task = (_title, _description, _dueDate, _priority, _completed) => {
  let title = _title;
  let description = _description;
  let dueDate = _dueDate;
  let priority = _priority;
  let completed = _completed;

  const getTitle = () => title;

  const getDescription = () => description;

  const getDueDate = () => dueDate;

  const getPriority = () => priority;

  const isCompleted = () => completed;

  const updateTitle = (newTitle) => {
    title = newTitle;
  };

  const updateDescription = (newDescription) => {
    description = newDescription;
  };

  const updateDueDate = (newDueDate) => {
    dueDate = newDueDate;
  };

  const updatePriority = (newPriority) => {
    priority = newPriority;
  };

  const toggleCompleted = () => {
    completed = !completed;
  };

  return Object.freeze({
    getTitle,
    getDescription,
    getDueDate,
    getPriority,
    isCompleted,
    updateTitle,
    updateDescription,
    updateDueDate,
    updatePriority,
    toggleCompleted,
  });
};

export const Project = (_title, _selected = false) => {
  const tasks = [];
  let title = _title;
  let selected = _selected;

  const getTasks = () => Object.freeze(tasks.slice());

  const getTask = (index) => Object.freeze(tasks[index]);

  const getTitle = () => title;

  const updateTitle = (newTitle) => {
    title = newTitle;
  };

  const addTask = (task) => {
    tasks.push(task);
  };

  const deleteTask = (index) => {
    tasks.splice(index, 1);
  };

  const updateTask = (index, task) => {
    tasks[index] = task;
  };

  const isSelected = () => selected;

  const setIsSelected = (value) => {
    selected = value;
  };

  const tasksCount = () => tasks.length;

  return Object.freeze({
    getTasks,
    getTask,
    getTitle,
    updateTitle,
    addTask,
    deleteTask,
    updateTask,
    isSelected,
    setIsSelected,
    tasksCount,
  });
};

export const TodoList = () => {
  const projects = [];
  let selectedProjectIndex = -1;

  const getProjects = () => Object.freeze(projects.slice());

  const getProject = (index) => Object.freeze(projects[index]);

  const projectsCount = () => projects.length;

  const addProject = (title, selected = false) => {
    if (selectedProjectIndex !== -1 && selectedProjectIndex < projects.length) {
      projects[selectedProjectIndex].setIsSelected(false);
    }
    projects.push(Project(title, selected));
    selectedProjectIndex = projectsCount() - 1;
  };

  const deleteProject = (index) => {
    projects.splice(index, 1);
    if (index < selectedProjectIndex) {
      selectedProjectIndex -= 1;
    } else if (index === selectedProjectIndex) {
      selectedProjectIndex = index === 0 ? -1 : 0;
    }
    if (selectedProjectIndex !== -1) {
      projects[selectedProjectIndex].setIsSelected(true);
    }
  };

  const updateProject = (index, title) => {
    projects[index].updateTitle(title);
  };

  const addTask = (projectIndex, task) => {
    projects[projectIndex].addTask(task);
  };

  const deleteTask = (projectIndex, taskIndex) => {
    projects[projectIndex].deleteTask(taskIndex);
  };

  const updateTask = (projectIndex, taskIndex, task) => {
    projects[projectIndex].updateTask(taskIndex, task);
  };

  const getSelectedProjectIndex = () => selectedProjectIndex;

  const setSelectedProjectIndex = (index) => {
    if (index === selectedProjectIndex) {
      return;
    }
    if (selectedProjectIndex !== -1) {
      projects[selectedProjectIndex].setIsSelected(false);
    }
    selectedProjectIndex = index;
    if (selectedProjectIndex !== -1) {
      projects[selectedProjectIndex].setIsSelected(true);
    }
  };

  return Object.freeze({
    getProjects,
    getProject,
    addProject,
    deleteProject,
    updateProject,
    projectsCount,
    addTask,
    deleteTask,
    updateTask,
    getSelectedProjectIndex,
    setSelectedProjectIndex,
  });
};
