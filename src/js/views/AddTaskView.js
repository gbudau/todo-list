const AddTaskView = () => {
  const parentElement = document.querySelector('.form__add_task');
  const modal = document.querySelector('.modal');
  const overlay = document.querySelector('.overlay');
  const btnOpen = document.querySelector('.tasks__btn_add');
  const btnClose = document.querySelector('.modal__close_button');

  const reset = () => {
    parentElement.reset();
    delete parentElement.dataset.editTaskIndex;
  };

  const setCustomValidity = (elementId, message) => {
    const input = parentElement.querySelector(`#${elementId}`);

    input.setCustomValidity(message);
  };

  const reportValidity = (elementId) => {
    const input = parentElement.querySelector(`#${elementId}`);

    input.reportValidity();
  };

  const toggleWindow = () => {
    modal.classList.toggle('hidden');
    overlay.classList.toggle('hidden');
  };

  const addHandlerShowWindow = () => {
    btnOpen.addEventListener('click', () => {
      reset();
      toggleWindow();
    });
  };

  const addHandlerCloseWindow = () => {
    overlay.addEventListener('click', () => {
      reset();
      toggleWindow();
    });

    btnClose.addEventListener('click', () => {
      reset();
      toggleWindow();
    });
  };

  const trimObject = (obj) => {
    const trimmed = JSON.stringify(obj, (key, value) => {
      if (typeof value === 'string') {
        return value.trim().replace(/\s+/g, ' ');
      }
      return value;
    });
    return JSON.parse(trimmed);
  };

  const addHandlerSubmit = (handler) => {
    parentElement.addEventListener('submit', (event) => {
      event.preventDefault();
      if (parentElement.dataset.editTaskIndex === undefined) {
        const formData = new FormData(event.target);
        const data = Object.fromEntries(formData);
        handler(trimObject(data));
        Object.keys(data).forEach((key) => {
          setCustomValidity(key, '');
        });
      }
    });
  };

  const addHandlerUpdate = (handler) => {
    parentElement.addEventListener('submit', (event) => {
      event.preventDefault();
      const taskIndex = parentElement.dataset.editTaskIndex;
      if (taskIndex) {
        const formData = new FormData(event.target);
        const data = Object.fromEntries(formData);
        handler(trimObject(data), Number(taskIndex));
        Object.keys(data).forEach((key) => {
          setCustomValidity(key, '');
        });
      }
    });
  };

  const fillForm = (task) => {
    const title = parentElement.querySelector('#title');
    const description = parentElement.querySelector('#description');
    const dueDate = parentElement.querySelector('#dueDate');
    const priority = parentElement.querySelector('#priority');

    title.value = task.getTitle();
    description.value = task.getDescription();
    dueDate.value = task.getDueDate();
    priority.value = task.getPriority();
  };

  addHandlerShowWindow();
  addHandlerCloseWindow();

  return {
    addHandlerSubmit,
    addHandlerUpdate,
    setCustomValidity,
    reportValidity,
    toggleWindow,
    fillForm,
  };
};

export default AddTaskView;
