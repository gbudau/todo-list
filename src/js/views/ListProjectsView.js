import IconEditProject from '../../images/pen-to-square-solid.svg';
import IconDeleteProject from '../../images/trash-can-solid.svg';
import IconAddProject from '../../images/plus-solid.svg';
import { PROJECTS_NAME } from '../config';

const ListProjectsView = () => {
  const parentElement = document.querySelector('.projects');

  const clear = () => {
    parentElement.replaceChildren();
  };

  const setCustomValidity = (message) => {
    const input = parentElement.querySelector('.projects__input');
    input.setCustomValidity(message);
  };

  const reportValidity = () => {
    const input = parentElement.querySelector('.projects__input');
    input.reportValidity();
  };

  const renderProjectsHeader = () => {
    const projectsHeader = document.createElement('div');
    projectsHeader.classList.add('projects__header');

    const projectsTitle = document.createElement('p');
    projectsTitle.classList.add('projects__title');
    projectsTitle.textContent = PROJECTS_NAME;

    projectsHeader.appendChild(projectsTitle);
    parentElement.appendChild(projectsHeader);
  };

  const renderProjects = (todolist) => {
    const projects = [];
    const list = document.createElement('ul');
    list.classList.add('projects__list');

    for (let i = 0; i < todolist.projectsCount(); i += 1) {
      const listItem = document.createElement('li');
      listItem.classList.add('projects__list_item');
      listItem.dataset.index = String(i);
      if (todolist.getProject(i).isSelected()) {
        listItem.classList.add('project__selected');
      }

      const projectTitle = document.createElement('span');
      projectTitle.classList.add('projects__project_title');
      projectTitle.textContent = todolist.getProject(i).getTitle();
      listItem.appendChild(projectTitle);

      if (i !== 0) {
        const editProjectBtn = document.createElement('input');
        editProjectBtn.setAttribute('type', 'image');
        editProjectBtn.src = IconEditProject;
        editProjectBtn.alt = 'Edit Project';
        editProjectBtn.classList.add('projects__edit_project');
        listItem.appendChild(editProjectBtn);

        const deleteProjectBtn = document.createElement('input');
        deleteProjectBtn.setAttribute('type', 'image');
        deleteProjectBtn.src = IconDeleteProject;
        deleteProjectBtn.alt = 'Delete Project';
        deleteProjectBtn.classList.add('projects__delete_project');
        listItem.appendChild(deleteProjectBtn);
      }

      projects.push(listItem);
    }

    list.append(...projects);
    parentElement.appendChild(list);
  };

  const renderAddProjectForm = () => {
    const form = document.createElement('form');
    form.classList.add(
      'projects__form_add_project',
      'projects__form_container',
    );

    const input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.setAttribute('placeholder', 'Add Project');
    input.setAttribute('required', 'required');
    input.classList.add('projects__input');

    const addProjectBtn = document.createElement('input');
    addProjectBtn.setAttribute('type', 'image');
    addProjectBtn.classList.add(
      'fa-solid',
      'fa-plus',
      'projects__input_submit',
    );
    addProjectBtn.src = IconAddProject;
    addProjectBtn.alt = 'Add Project';

    form.appendChild(input);
    form.appendChild(addProjectBtn);
    parentElement.appendChild(form);
  };

  const clearAddProjectQuery = () => {
    const input = parentElement.querySelector('.projects__input');
    input.value = '';
  };

  const addHandlerSelectProject = (handler) => {
    parentElement.addEventListener('click', (event) => {
      if (event.target.classList.contains('projects__delete_project')) {
        return;
      }

      const projectTitleElement = event.target.closest('.projects__list_item');
      if (projectTitleElement) {
        handler(Number(projectTitleElement.dataset.index));

        const form = parentElement.querySelector(
          '.projects__form_add_project',
        );
        clearAddProjectQuery();
        delete form.dataset.editProjectIndex;
      }
    });
  };

  const setProjectSelected = (index) => {
    const projectListItem = parentElement.querySelector(
      `[data-index="${String(index)}"]`,
    );

    if (projectListItem) {
      projectListItem.classList.add('project__selected');
    }
  };

  const unsetProjectSelected = (index) => {
    const projectListItem = parentElement.querySelector(
      `[data-index="${String(index)}"]`,
    );

    if (projectListItem) {
      projectListItem.classList.remove('project__selected');
    }
  };

  const addHandlerDeleteProject = (handler) => {
    parentElement.addEventListener('click', (event) => {
      if (!event.target.classList.contains('projects__delete_project')) {
        return;
      }
      const projectListItem = event.target.closest('.projects__list_item');

      if (projectListItem) {
        handler(Number(projectListItem.dataset.index));
      }
    });
  };

  const getAddProjectQuery = () => {
    const input = parentElement.querySelector('.projects__input');
    const value = input.value.trim().replace(/\s+/g, ' ');
    clearAddProjectQuery();
    return value;
  };

  const addHandlerAddProject = (handler) => {
    parentElement.addEventListener('submit', (event) => {
      event.preventDefault();
      const form = parentElement.querySelector('.projects__form_add_project');
      if (form.dataset.editProjectIndex === undefined) {
        handler();
        setCustomValidity('');
      }
    });
  };

  const addHandlerEditProject = (handler) => {
    parentElement.addEventListener('click', (event) => {
      if (!event.target.classList.contains('projects__edit_project')) {
        return;
      }

      const projectListItem = event.target.closest('.projects__list_item');

      if (projectListItem) {
        const form = parentElement.querySelector(
          '.projects__form_add_project',
        );
        form.dataset.editProjectIndex = projectListItem.dataset.index;
        handler(Number(projectListItem.dataset.index));
      }
    });
  };

  const addHandlerUpdateProject = (handler) => {
    parentElement.addEventListener('submit', (event) => {
      event.preventDefault();
      const form = parentElement.querySelector('.projects__form_add_project');
      if (form.dataset.editProjectIndex) {
        handler(Number(form.dataset.editProjectIndex));
        setCustomValidity('');
      }
    });
  };

  const fillForm = (projectTitle) => {
    const form = parentElement.querySelector('.projects__form_add_project');
    const input = form.querySelector('.projects__input');

    input.value = projectTitle;
  };

  const render = (todolist) => {
    clear();
    renderProjectsHeader();
    renderProjects(todolist);
    renderAddProjectForm();
  };

  return {
    render,
    addHandlerSelectProject,
    setProjectSelected,
    unsetProjectSelected,
    addHandlerDeleteProject,
    addHandlerAddProject,
    addHandlerEditProject,
    addHandlerUpdateProject,
    fillForm,
    getAddProjectQuery,
    setCustomValidity,
    reportValidity,
  };
};

export default ListProjectsView;
