import IconDeleteTask from '../../images/trash-can-solid.svg';
import IconEditTask from '../../images/pen-to-square-solid.svg';

const TasksView = () => {
  const parentElement = document.querySelector('.preview_project');

  const clear = () => {
    parentElement.replaceChildren();
  };

  const renderProjectTitle = (project) => {
    const header = document.createElement('div');
    header.classList.add('preview_project__header');

    const title = document.createElement('p');
    title.classList.add('preview_project__title');
    title.textContent = project.getTitle();

    header.appendChild(title);

    parentElement.appendChild(header);
  };

  const createTaskItem = (index) => {
    const taskItem = document.createElement('li');

    taskItem.classList.add('preview_tasks__task');
    taskItem.dataset.index = String(index);

    return taskItem;
  };

  const createTaskComplete = (task) => {
    const taskComplete = document.createElement('input');

    taskComplete.setAttribute('type', 'checkbox');
    taskComplete.classList.add('task-checkbox');
    if (task.isCompleted()) {
      taskComplete.setAttribute('checked', 'true');
    }

    return taskComplete;
  };

  const createTaskTitle = (task) => {
    const taskTitle = document.createElement('span');

    taskTitle.classList.add('preview_tasks__task_title');
    taskTitle.textContent = task.getTitle();
    if (task.isCompleted()) {
      taskTitle.classList.add('task-complete');
    }

    return taskTitle;
  };

  const createTaskEditBtn = () => {
    const taskEditBtn = document.createElement('input');
    taskEditBtn.setAttribute('type', 'image');
    taskEditBtn.src = IconEditTask;
    taskEditBtn.alt = 'Edit Task';
    taskEditBtn.classList.add('task_edit_btn');

    return taskEditBtn;
  };

  const createTaskPriority = (task) => {
    const taskPriority = document.createElement('span');

    taskPriority.classList.add('task-priority');
    const priority = task.getPriority();
    if (priority === 'low') {
      taskPriority.classList.add('priority-low');
    } else if (priority === 'medium') {
      taskPriority.classList.add('priority-medium');
    } else {
      taskPriority.classList.add('priority-high');
    }

    return taskPriority;
  };

  const createTaskDeleteBtn = () => {
    const taskDeleteBtn = document.createElement('input');
    taskDeleteBtn.setAttribute('type', 'image');
    taskDeleteBtn.src = IconDeleteTask;
    taskDeleteBtn.alt = 'Delete Task';
    taskDeleteBtn.classList.add('preview_tasks__delete_task');

    return taskDeleteBtn;
  };

  const renderTasks = (project) => {
    const tasksList = document.createElement('ul');
    tasksList.classList.add('preview_tasks__list');

    const tasks = [];

    for (let i = 0; i < project.tasksCount(); i += 1) {
      const task = project.getTask(i);
      const taskItem = createTaskItem(i);

      taskItem.appendChild(createTaskComplete(task));
      taskItem.appendChild(createTaskPriority(task));
      taskItem.appendChild(createTaskTitle(task));
      taskItem.appendChild(createTaskEditBtn());
      taskItem.appendChild(createTaskDeleteBtn());
      tasks.push(taskItem);
    }

    tasksList.append(...tasks);
    parentElement.appendChild(tasksList);
  };

  const addHandlerDeleteTask = (handler) => {
    parentElement.addEventListener('click', (event) => {
      if (event.target.classList.contains('preview_tasks__delete_task')) {
        const taskListItem = event.target.closest('.preview_tasks__task');

        if (taskListItem) {
          handler(taskListItem.dataset.index);
        }
      }
    });
  };

  const addHandlerCompleteTask = (handler) => {
    parentElement.addEventListener('click', (event) => {
      if (event.target.classList.contains('task-checkbox')) {
        const taskListItem = event.target.closest('.preview_tasks__task');

        if (taskListItem) {
          const taskTitle = taskListItem.querySelector(
            '.preview_tasks__task_title',
          );
          taskTitle.classList.toggle('task-complete');
          handler(taskListItem.dataset.index);
        }
      }
    });
  };

  const addHandlerEditTask = (handler) => {
    parentElement.addEventListener('click', (event) => {
      if (event.target.classList.contains('task_edit_btn')) {
        const taskListItem = event.target.closest('.preview_tasks__task');

        if (taskListItem) {
          const form = document.querySelector('.form__add_task');
          form.dataset.editTaskIndex = taskListItem.dataset.index;
          handler(taskListItem.dataset.index);
        }
      }
    });
  };

  const render = (project) => {
    clear();
    renderProjectTitle(project);
    renderTasks(project);
  };

  return {
    render,
    addHandlerDeleteTask,
    addHandlerCompleteTask,
    addHandlerEditTask,
  };
};

export default TasksView;
