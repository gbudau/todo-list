export const isPositiveInteger = (value) => Number.isInteger(value) && value > 0;

export function updateCopyrightYear(selector) {
  const copyrightYear = document.querySelector(selector);
  const today = new Date();

  copyrightYear.textContent = today.getFullYear();
}
